package com.diksha.samplerepo;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.util.Random;

public class MainActivityDataGenerator extends ViewModel {

    private String TAG = "LiveData==>";
    private MutableLiveData<String> randomNumber;

    public MutableLiveData<String> createRandomNumber() {
        Log.d(TAG, "Get Number");
        if (randomNumber == null) {
            randomNumber = new MutableLiveData<>();
            generateRandomNumber();
        }
        return randomNumber;
    }

    public void generateRandomNumber() {
        Log.d(TAG, "CreateNewNumber");
        Random random = new Random();
        randomNumber.setValue("number:" + (random.nextInt(10 - 1) +1));
    }


    @Override
    protected void onCleared() {
        super.onCleared();
        Log.d(TAG, "ViewModel Cleared");
    }
}
