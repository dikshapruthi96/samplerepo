package com.diksha.samplerepo;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.diksha.samplerepo.databinding.ActivityMainBinding;

import java.util.Random;

public class MainActivity extends AppCompatActivity {
    private ActivityMainBinding binding;
    private String TAG = "LiveData==>";
    public MainActivityDataGenerator model;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
      //  MainActivityDataGenerator myData = new MainActivityDataGenerator();
        model = ViewModelProviders.of(this).get(MainActivityDataGenerator.class);
        binding.setViewModel(model);
      //  String number = myData.createRandomNumber();
        MutableLiveData<String> number = model.createRandomNumber();

        number.observe(this, new Observer<String>() {
            @Override
            public void onChanged(String s) {
                Log.d(TAG, "data updated on UI");
                binding.tvRandomNumber.setText(s);
            }
        });

        Handler handler = new Handler();
        binding.setCallback(handler);
    }
}